
import axios from 'axios'

export default () => {
    axios.defaults.baseURL = 'http://api.slavovdev.eu/';
    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

    var token = localStorage.getItem('token');
    if(token){
        axios.defaults.headers.common['Authorization'] = token;
    }
}


