import axios from "axios"
import 'es6-promise/auto'

export const authenticate = {
    methods: {
        login() {

            axios.post('auth/login', {
                    'email': this.email,
                    'password': this.password
                })
                .then(data => {
                    this.$store.commit('update', {
                        'target': 'token',
                        'value': data.token
                    });
                    this.$store.commit('update', {
                        'target': 'userName',
                        'value': data.name
                    });
                    axios.defaults.headers.common['token'] = data.token;
                });



        },
        register() {

            axios.post('auth/register', {
                    'name': this.name,
                    'email': this.email,
                    'password': this.password
                })
                .then(data => {
                    this.$store.commit('update', {
                        'target': 'token',
                        'value': data.token
                    });
                    this.$store.commit('update', {
                        'target': 'userName',
                        'value': data.name
                    });
                    axios.defaults.headers.common['token'] = data.token;
                });


        },

        logout() {
            this.$store.commit('update', {
                'target': 'token',
                'value': ""
            });
            this.$store.commit('update', {
                'target': 'userName',
                'value': ""
            });
            delete axios.defaults.headers.common['token'];
        },
    },
    computed: {
        // For cleaner tamplate usage
        isAuthenticated() {
            return this.$store.getters.isLoggedIn;
        }
    }


}