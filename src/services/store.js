import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


const localStoragePlugin = store => {
    store.subscribe((mutation,  state ) => {
      window.localStorage.setItem('store', JSON.stringify(state))
    })
}

export default new Vuex.Store({
    state: {
        token: '',
        userName: ''
    },
    plugins: [localStoragePlugin],
    mutations:{
        update (state, updateData){
            state[updateData.target] = updateData.value;
        },
        initialiseStore(state) {
			// Check if the ID exists
			if(localStorage.getItem('store')) {
				// Replace the state object with the stored item
				this.replaceState(
					Object.assign(state, JSON.parse(localStorage.getItem('store')))
				);
			}
		}
    },
    getters:{
        isLoggedIn(state){
            return state.token !== '';
        }
    }
});