import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuelidate from 'vuelidate'
import 'bulma/css/bulma.css'
import store from '@/services/store'


import config from '@/config/config'

// @ is an alias to /src
import header from './components/HeaderPart'
import footer from './components/FooterPart'

Vue.use(Vuelidate)

config();

Vue.config.productionTip = false


Vue.component('header-part', header);
Vue.component('footer-part', footer);

new Vue({
  router,
  render: h => h(App),
  store,
  beforeCreate() {
		this.$store.commit('initialiseStore');
	}
}).$mount('#app')
