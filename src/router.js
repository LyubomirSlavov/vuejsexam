import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home'
import Login from './views/auth/Login'
import Register from './views/auth/Register'
import NotFound from './views/404'
import AdminPanel from './views/admin/Panel'
import PagePostForm from './views/admin/PagePostForm'

Vue.use(Router)


const loginGuard = (to, from, next) => {
    var store = JSON.parse(localStorage.getItem('store'));
    if(store.token !== ''){
      next();
    }else{
      next(false);
    }
}


export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path:'/admin/panel',
      name: 'panel',
      component: AdminPanel,
      beforeEnter: loginGuard
    },
    {
      path: '/page/create',
      component: PagePostForm,
      name: 'pageform',
      beforeEnter: loginGuard
    },
    {
      path: '/post/create',
      component: PagePostForm,
      name: 'postform',
      beforeEnter: loginGuard
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      // will match everything
      path: '*',
      name: '404',
      component: NotFound
    }
  ]
})
